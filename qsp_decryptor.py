import argparse
import os


def ultimate_qsp_password_decryptor(path, verbose=False):

    # open file and get encoded password
    with open(path, 'r', encoding='utf_16_le') as my_file:
        qsp_strings = my_file.readlines()  # put every string in the list
    y1 = qsp_strings[2]  # type is string
    y  = y1[:-1]  # cut off unnecessary characters

    # show debug info
    if verbose:
        print(f'String length: {len(y)}\n'
              f'Encoded data:\n{y}\n'
              f'Unicode code points:')
        for elem in y:
            print(ord(elem) + 5, end=',')
        print()

    # handling empty password
    if y == 'Ij':
        return None

    # decoding password
    z = ''
    for elem in y:
        z += chr(ord(elem) + 5)
    return z


if __name__ == '__main__':

    # command line options handling
    default_path = os.path.normpath(os.path.dirname(__file__) + '/game.qsp')
    parser = argparse.ArgumentParser(description='QSP Password Decryptor. Decode qsp files passwords')
    parser.add_argument('-debug',
                        help='show debug info',
                        action="store_true")
    parser.add_argument('file_path',
                        help='specify path to file, which have to be decoded',
                        default=default_path, nargs='?')
    args = parser.parse_args()

    password = ultimate_qsp_password_decryptor(args.file_path, verbose=args.debug)

    if password is None:
        print('This file has no password!')
    else:
        print('Password is:', password)
