#!/usr/bin/python3

from qsp_decryptor import ultimate_qsp_password_decryptor as qd

passwords = {'game.qsp': '1234АБВГ',
             'game_2.qsp': r'!@#$%^&*()_+=-0987654321',
             'game_3.qsp': '如果我在你身邊，夕陽非常美麗',
             'game_4.qsp': 'test_password',
             'game_1111.qsp': '1111'}

no_passwords = {'game_nopass.qsp': None,
                'game_nopass2.qsp': None,
                'game_nopass3.qsp': None}


def test():
    for key in passwords:
        if qd(key, verbose=False) == passwords[key]:
            print("{}: Password match - ok".format(key))
        else:
            print('Error: no match')

    for key in no_passwords:
        if qd(key, verbose=False) is None:
            print("{}: File has no password - ok".format(key))


if __name__ == "__main__":
    test()
    print("\nEverything is ok")
